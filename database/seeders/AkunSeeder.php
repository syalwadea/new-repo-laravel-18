<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= [
            [
                'username' => 'manajer1',
                'name' => 'manajer1',
                'email' => 'manajer1@gmail.com',
                'level' => 'manajer',
                'password' => 'manajer123'
            ],
            [
                'username' => 'cs1',
                'name' => 'cs1',
                'email' => 'cs1@gmail.com',
                'level' => 'cs',
                'password' => 'cs123'
            ]
            ];
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
